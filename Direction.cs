﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Microsoft.Samples.Kinect.SpeechBasics
{
    /// <summary>
    /// Enumeration of directions in which turtle may be facing.
    /// </summary>
    public enum Direction
    {
        /// <summary>
        /// Represents going up
        /// </summary>
        Up,

        /// <summary>
        /// Represents going down
        /// </summary>
        Down,

        /// <summary>
        /// Represents going left
        /// </summary>
        Left,

        /// <summary>
        /// Represents going right
        /// </summary>
        Right,
        Circle,
        Rectangle,
        ZigZag
    }

}
