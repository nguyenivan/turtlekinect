﻿//------------------------------------------------------------------------------
// <copyright file="GestureDetector.cs" company="Microsoft">
//     Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

namespace Microsoft.Samples.Kinect.SpeechBasics
{
    using System;
    using System.Collections.Generic;
    using Microsoft.Kinect;
    using Microsoft.Kinect.VisualGestureBuilder;
    using System.Linq;

    /// <summary>
    /// Gesture Detector class which listens for VisualGestureBuilderFrame events from the service
    /// and updates the associated GestureResultView object with the latest results for the 'Seated' gesture
    /// </summary>
    public class GestureDetector : IDisposable
    {
        private readonly Dictionary<string, Direction> directionMap = new Dictionary<string, Direction>(){
            {"MoveUp", Direction.Up},
            {"MoveDown", Direction.Down },
            {"MoveLeft", Direction.Left },
            {"MoveRight", Direction.Right },
            {"Circle", Direction.Circle},
            {"ZigZag", Direction.ZigZag},
            {"Rectangle", Direction.Rectangle},
        };

        private Dictionary<string, bool> resetDict = new Dictionary<string, bool>()
        {
            {"Circle", false},
            {"Rectangle", false},
            {"ZigZag", false},
        };

        private Dictionary<string, int> progressDict = new Dictionary<string, int>()
        {
            {"Circle", 0},
            {"Rectangle", 0},
            {"ZigZag", 0},
        };

        /// <summary> Gesture frame source which should be tied to a body tracking ID </summary>
        private VisualGestureBuilderFrameSource vgbFrameSource = null;

        /// <summary> Gesture frame reader which will handle gesture events coming from the sensor </summary>
        private VisualGestureBuilderFrameReader vgbFrameReader = null;
        private float threshold = 0.8f;


        public event MoveEventHandler MoveGestureMatched = delegate { };
        private DateTime lastMatch = default(DateTime);
        private TimeSpan matchGap = new TimeSpan(0, 0, 1);


        /// <summary>
        /// Initializes a new instance of the GestureDetector class along with the gesture frame source and reader
        /// </summary>
        /// <param name="kinectSensor">Active sensor to initialize the VisualGestureBuilderFrameSource object with</param>
        /// <param name="gestureResultView">GestureResultView object to store gesture results of a single body to</param>
        public GestureDetector(KinectSensor kinectSensor)
        {
            if (kinectSensor == null)
            {
                throw new ArgumentNullException("kinectSensor");
            }

            // create the vgb source. The associated body tracking ID will be set when a valid body frame arrives from the sensor.
            this.vgbFrameSource = new VisualGestureBuilderFrameSource(kinectSensor, 0);
            this.vgbFrameSource.TrackingIdLost += this.Source_TrackingIdLost;

            // open the reader for the vgb frames
            this.vgbFrameReader = this.vgbFrameSource.OpenReader();
            if (this.vgbFrameReader != null)
            {
                this.vgbFrameReader.IsPaused = true;
                this.vgbFrameReader.FrameArrived += this.Reader_GestureFrameArrived;
            }

            // load the gesture from the gesture database
            using (VisualGestureBuilderDatabase database = new VisualGestureBuilderDatabase(@"Database/TurtleControl.gbd"))
            {
                // we could load all available gestures in the database with a call to vgbFrameSource.AddGestures(database.AvailableGestures), 
                // but for this program, we only want to track one discrete gesture from the database, so we'll load it by name
                foreach (Gesture gesture in database.AvailableGestures)
                {
                    this.vgbFrameSource.AddGesture(gesture);
                }
            }

        }

        /// <summary>
        /// Gets or sets the body tracking ID associated with the current detector
        /// The tracking ID can change whenever a body comes in/out of scope
        /// </summary>
        public ulong TrackingId
        {
            get
            {
                return this.vgbFrameSource.TrackingId;
            }

            set
            {
                if (this.vgbFrameSource.TrackingId != value)
                {
                    this.vgbFrameSource.TrackingId = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not the detector is currently paused
        /// If the body tracking ID associated with the detector is not valid, then the detector should be paused
        /// </summary>
        public bool IsPaused
        {
            get
            {
                return this.vgbFrameReader.IsPaused;
            }

            set
            {
                if (this.vgbFrameReader.IsPaused != value)
                {
                    this.vgbFrameReader.IsPaused = value;
                }
            }
        }

        /// <summary>
        /// Disposes all unmanaged resources for the class
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Disposes the VisualGestureBuilderFrameSource and VisualGestureBuilderFrameReader objects
        /// </summary>
        /// <param name="disposing">True if Dispose was called directly, false if the GC handles the disposing</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (this.vgbFrameReader != null)
                {
                    this.vgbFrameReader.FrameArrived -= this.Reader_GestureFrameArrived;
                    this.vgbFrameReader.Dispose();
                    this.vgbFrameReader = null;
                }

                if (this.vgbFrameSource != null)
                {
                    this.vgbFrameSource.TrackingIdLost -= this.Source_TrackingIdLost;
                    this.vgbFrameSource.Dispose();
                    this.vgbFrameSource = null;
                }
            }
        }

        /// <summary>
        /// Handles gesture detection results arriving from the sensor for the associated body tracking Id
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void Reader_GestureFrameArrived(object sender, VisualGestureBuilderFrameArrivedEventArgs e)
        {
            VisualGestureBuilderFrameReference frameReference = e.FrameReference;
            using (VisualGestureBuilderFrame frame = frameReference.AcquireFrame())
            {
                if (frame != null)
                {
                    // get the discrete gesture results which arrived with the latest frame
                    IReadOnlyDictionary<Gesture, DiscreteGestureResult> discreteResults = frame.DiscreteGestureResults;
                    if (discreteResults != null)
                    {
                        // we only have one gesture in this source object, but you can get multiple gestures
                        foreach (Gesture gesture in this.vgbFrameSource.Gestures)
                        {
                            if (gesture.GestureType == GestureType.Discrete)
                            {
                                DiscreteGestureResult result = null;
                                discreteResults.TryGetValue(gesture, out result);

                                DateTime now = DateTime.Now;
                                TimeSpan gap = now - this.lastMatch;
                                if (gap > this.matchGap && result != null && result.Detected && result.Confidence >= this.threshold)
                                {
                                    // update the GestureResultView object with new gesture result values
                                    if (this.directionMap.ContainsKey(gesture.Name))
                                    {
                                        this.MoveGestureMatched(this, new MoveEventArgs(this.directionMap[gesture.Name]));
                                        this.lastMatch = DateTime.Now;
                                        return;
                                    }
                                }
                            }
                        }
                    }

                    IReadOnlyDictionary<Gesture, ContinuousGestureResult> continousResults = frame.ContinuousGestureResults;
                    if (continousResults != null)
                    {
                        // we only have one gesture in this source object, but you can get multiple gestures
                        foreach (Gesture gesture in this.vgbFrameSource.Gestures)
                        {
                            if (gesture.GestureType == GestureType.Continuous)
                            {
                                ContinuousGestureResult cResult = null;
                                continousResults.TryGetValue(gesture, out cResult);

                                //if (cResult.Progress < 0.3f)
                                //{
                                //    this.resetDict[gesture.Name] = true;
                                //}

                                //if (this.resetDict[gesture.Name] && cResult.Progress > this.threshold)
                                //{
                                //    this.resetDict[gesture.Name] = false;
                                //    DateTime now = DateTime.Now;
                                //    TimeSpan gap = now - this.lastMatch;
                                //    if (gap > this.matchGap && cResult != null)
                                //    {
                                //        // update the GestureResultView object with new gesture result values
                                //        if (this.directionMap.ContainsKey(gesture.Name))
                                //        {
                                //            this.MoveGestureMatched(this, new MoveEventArgs(this.directionMap[gesture.Name]));
                                //            this.lastMatch = DateTime.Now;
                                //            //foreach (string k in this.resetDict.Keys.ToList())
                                //            //{
                                //            //    this.resetDict[k] = false;
                                //            //}
                                //            return;
                                //        }
                                //    }
                                //}
                                int currentProgress = (int)Math.Floor(cResult.Progress/0.1);
                                if (this.progressDict[gesture.Name] > currentProgress)
                                {
                                    this.progressDict[gesture.Name] = currentProgress; 
                                }
                                else if (this.progressDict[gesture.Name] + 1 == currentProgress)
                                {
                                    this.progressDict[gesture.Name] = currentProgress;
                                    if (this.progressDict[gesture.Name] >= 8 && cResult.Progress >= this.threshold)
                                    {
                                        foreach (string k in this.progressDict.Keys.ToList())
                                        {
                                            this.progressDict[k] = 4;
                                        }
                                        //this.progressDict[gesture.Name] = 0;
                                        DateTime now = DateTime.Now;
                                        TimeSpan gap = now - this.lastMatch;
                                        if (gap > this.matchGap)
                                        {
                                            this.lastMatch = now;
                                            this.MoveGestureMatched(this, new MoveEventArgs(this.directionMap[gesture.Name]));
                                        }
                                    }
                                }
                                
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Handles the TrackingIdLost event for the VisualGestureBuilderSource object
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void Source_TrackingIdLost(object sender, TrackingIdLostEventArgs e)
        {
            // update the GestureResultView object to show the 'Not Tracked' image in the UI
        }


    }
}
