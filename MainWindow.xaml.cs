﻿//------------------------------------------------------------------------------
// <copyright file="MainWindow.xaml.cs" company="Microsoft">
//     Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

namespace Microsoft.Samples.Kinect.SpeechBasics
{
    using System;
    using System.Collections.Generic;    
    using System.ComponentModel;
    using System.IO;
    using System.Runtime.InteropServices;
    using System.Text;    
    using System.Windows;    
    using System.Windows.Documents;
    using System.Windows.Media;
    using Microsoft.Kinect;
    using System.Windows.Threading;

    /// <summary>
    /// Interaction logic for MainWindow
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// Resource key for medium-gray-colored brush.
        /// </summary>
        private const string MediumGreyBrushKey = "MediumGreyBrush";

        /// <summary>
        /// Map between each direction and the direction immediately to its right.
        /// </summary>
        private static readonly Dictionary<Direction, Direction> TurnRight = new Dictionary<Direction, Direction>
            {
                { Direction.Up, Direction.Right },
                { Direction.Right, Direction.Down },
                { Direction.Down, Direction.Left },
                { Direction.Left, Direction.Up }
            };

        /// <summary>
        /// Map between each direction and the direction immediately to its left.
        /// </summary>
        private static readonly Dictionary<Direction, Direction> TurnLeft = new Dictionary<Direction, Direction>
            {
                { Direction.Up, Direction.Left },
                { Direction.Right, Direction.Up },
                { Direction.Down, Direction.Right },
                { Direction.Left, Direction.Down }
            };

        /// <summary>
        /// Map between each direction and the displacement unit it represents.
        /// </summary>
        private static readonly Dictionary<Direction, Point> Displacements = new Dictionary<Direction, Point>
            {
                { Direction.Up, new Point { X = 0, Y = -1 } },
                { Direction.Right, new Point { X = 1, Y = 0 } },
                { Direction.Down, new Point { X = 0, Y = 1 } },
                { Direction.Left, new Point { X = -1, Y = 0 } }
            };

        /// <summary>
        /// Active Kinect sensor.
        /// </summary>
        private KinectSensor kinectSensor = null;

        /// <summary>
        /// Current direction where turtle is facing.
        /// </summary>
        private Direction curDirection = Direction.Up;

        /// <summary>
        /// List of all UI span elements used to select recognized text.
        /// </summary>
        private List<Span> recognitionSpans;
        private BodyFrameReader bodyFrameReader = null;
        private GestureDetector gestureDetector = null;
        private Body[] bodies = null;
        private KinectBodyView kinectBodyView = null;
        private readonly int CENTER_X = 300;
        private readonly int CENTER_Y = 200;
        private bool turtleCentering = false;
        private bool turtleRectangle = false;
        private bool turtleZigZag = false;
        private int rectangleCount=1;
        private int zigzagCount=1;

        /// <summary>
        /// Initializes a new instance of the MainWindow class.
        /// </summary>
        public MainWindow()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Execute initialization tasks.
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void WindowLoaded(object sender, RoutedEventArgs e)
        {
            // Only one sensor is supported
            this.kinectSensor = KinectSensor.GetDefault();

            if (this.kinectSensor != null)
            {
                // open the sensor
                this.kinectSensor.Open();
                // open the reader for the body frames
                this.bodyFrameReader = this.kinectSensor.BodyFrameSource.OpenReader();
                // set the BodyFramedArrived event notifier
                this.bodyFrameReader.FrameArrived += this.Reader_BodyFrameArrived;
                this.gestureDetector = new GestureDetector(this.kinectSensor);
                this.gestureDetector.MoveGestureMatched += gestureDetector_MoveGestureMatched;

                // initialize the BodyViewer object for displaying tracked bodies in the UI
                this.kinectBodyView = new KinectBodyView(this.kinectSensor);
                // set our data context objects for display in UI
                this.DataContext = this;
                this.kinectBodyViewbox.DataContext = this.kinectBodyView;
            }
            else
            {
                // on failure, set the status text
                this.statusBarText.Text = Properties.Resources.NoKinectReady;
                return;
            }

            DispatcherTimer timer = new DispatcherTimer();
            timer.Tick += new EventHandler(timer_Tick);
            timer.Interval = new TimeSpan(0, 0, 0, 0, 300);
            timer.Start();

            this.recognitionSpans = new List<Span> { forwardSpan, backSpan, rightSpan, leftSpan,circleSpan, rectangleSpan, zigzagSpan };
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            if (this.turtleCentering)
            {
                this.CenterTurtle();
            }
            if (this.turtleRectangle)
            {
                this.RectangleTurtle(this.rectangleCount); this.rectangleCount++;
            }
            if (this.turtleZigZag)
            {
                this.ZigZagTurtle(this.zigzagCount); this.zigzagCount++;
            }
        }

        private void gestureDetector_MoveGestureMatched(object sender, MoveEventArgs e)
        {
            this.MoveTurtle(e.Verb);
        }

        private void Reader_BodyFrameArrived(object sender, BodyFrameArrivedEventArgs e)
        {
            bool dataReceived = false;

            using (BodyFrame bodyFrame = e.FrameReference.AcquireFrame())
            {
                if (bodyFrame != null)
                {
                    if (this.bodies == null)
                    {
                        // creates an array of 6 bodies, which is the max number of bodies that Kinect can track simultaneously
                        this.bodies = new Body[bodyFrame.BodyCount];
                    }

                    // The first time GetAndRefreshBodyData is called, Kinect will allocate each Body in the array.
                    // As long as those body objects are not disposed and not set to null in the array,
                    // those body objects will be re-used.
                    bodyFrame.GetAndRefreshBodyData(this.bodies);
                    dataReceived = true;
                }
            }

            if (dataReceived)
            {
                // visualize the new body data
                this.kinectBodyView.UpdateBodyFrame(this.bodies);

                // we may have lost/acquired bodies, so update the corresponding gesture detectors
                if (this.bodies != null)
                {
                    // loop through all bodies to see if any of the gesture detectors need to be updated
                    int maxBodies = this.kinectSensor.BodyFrameSource.BodyCount;
                    ulong currentTrackingId = 0;
                    for (int i = 0; i < maxBodies; ++i)
                    {
                        Body body = this.bodies[i];
                        if (body.TrackingId != 0 && (
                            body.TrackingId == this.gestureDetector.TrackingId || this.gestureDetector.TrackingId == 0))
                        {
                            currentTrackingId = body.TrackingId;
                            break;
                        }
                    }
                    this.gestureDetector.IsPaused = currentTrackingId == 0;
                    this.gestureDetector.TrackingId = currentTrackingId;
                }
            }
        }

        /// <summary>
        /// Execute un-initialization tasks.
        /// </summary>
        /// <param name="sender">object sending the event.</param>
        /// <param name="e">event arguments.</param>
        private void WindowClosing(object sender, CancelEventArgs e)
        {
            if (this.bodyFrameReader != null)
            {
                // BodyFrameReader is IDisposable
                this.bodyFrameReader.FrameArrived -= this.Reader_BodyFrameArrived;
                this.bodyFrameReader.Dispose();
                this.bodyFrameReader = null;
            }

            this.gestureDetector.Dispose();
            this.gestureDetector.MoveGestureMatched -= this.gestureDetector_MoveGestureMatched;
            if (null != this.kinectSensor)
            {
                this.kinectSensor.Close();
                this.kinectSensor = null;
            }
        }

        /// <summary>
        /// Remove any highlighting from recognition instructions.
        /// </summary>
        private void ClearRecognitionHighlights()
        {
            foreach (Span span in this.recognitionSpans)
            {
                span.Foreground = (Brush)this.Resources[MediumGreyBrushKey];
                span.FontWeight = FontWeights.Normal;
            }
        }

        /// <summary>
        /// Handler for recognized speech events.
        /// </summary>
        /// <param name="sender">object sending the event.</param>
        /// <param name="e">event arguments.</param>
        private void MoveTurtle(Direction verb)
        {
            // Number of degrees in a right angle.
            const int DegreesInRightAngle = 90;

            // Number of pixels turtle should move forwards or backwards each time.
            const int DisplacementAmount = 60;

            this.ClearRecognitionHighlights();

            switch (verb)
            {
                case Direction.Up:
                    forwardSpan.Foreground = Brushes.DeepSkyBlue;
                    forwardSpan.FontWeight = FontWeights.Bold;
                    turtleRotation.Angle =  0*DegreesInRightAngle;
                    turtleTranslation.Y = (playArea.Height + turtleTranslation.Y - DisplacementAmount) % playArea.Height;
                    break;

                case Direction.Down:
                    backSpan.Foreground = Brushes.DeepSkyBlue;
                    backSpan.FontWeight = FontWeights.Bold;
                    turtleRotation.Angle =  2*DegreesInRightAngle;
                    turtleTranslation.Y = (playArea.Height + turtleTranslation.Y + DisplacementAmount) % playArea.Height;
                    break;

                case Direction.Left:
                    leftSpan.Foreground = Brushes.DeepSkyBlue;
                    leftSpan.FontWeight = FontWeights.Bold;
                    // We take a right turn to mean a clockwise right angle rotation for the displayed turtle.
                    turtleRotation.Angle = -DegreesInRightAngle;
                    turtleTranslation.X = (playArea.Width + turtleTranslation.X - DisplacementAmount) % playArea.Width;

                    break;

                case Direction.Right:
                    rightSpan.Foreground = Brushes.DeepSkyBlue;
                    rightSpan.FontWeight = FontWeights.Bold;
                    this.curDirection = TurnRight[this.curDirection];
                    // We take a right turn to mean a clockwise right angle rotation for the displayed turtle.
                    turtleRotation.Angle = DegreesInRightAngle;
                    turtleTranslation.X = (playArea.Width + turtleTranslation.X + DisplacementAmount) % playArea.Width;
                    break;

                case Direction.Circle:
                    circleSpan.Foreground = Brushes.DeepSkyBlue;
                    circleSpan.FontWeight = FontWeights.Bold;
                    this.turtleCentering = true;
                    break;
                case Direction.ZigZag:
                    zigzagSpan.Foreground = Brushes.DeepSkyBlue;
                    zigzagSpan.FontWeight = FontWeights.Bold;
                    this.turtleZigZag = true;
                    break;
                case Direction.Rectangle:
                    rectangleSpan.Foreground = Brushes.DeepSkyBlue;
                    rectangleSpan.FontWeight = FontWeights.Bold;
                    this.turtleRectangle = true;
                    break;

            }
        }

        private void CenterTurtle()
        {
            int DegreesInRightAngle = 90;
            int DisplacementAmount = 60;
            var s = Math.Sign(turtleTranslation.X - this.CENTER_X);
            turtleRotation.Angle = -s * DegreesInRightAngle;
            while (Math.Abs(turtleTranslation.X - this.CENTER_X) > DisplacementAmount)
            {
                turtleTranslation.X = (playArea.Width + turtleTranslation.X - s * DisplacementAmount) % playArea.Width;
                return;
            }
            if (turtleTranslation.X != this.CENTER_X)
            {
                turtleTranslation.X = this.CENTER_X;
                return;
            }

            s = Math.Sign(turtleTranslation.Y - this.CENTER_Y);
            turtleRotation.Angle = 90 + s * DegreesInRightAngle;
            while (Math.Abs(turtleTranslation.Y - this.CENTER_Y) > DisplacementAmount)
            {
                turtleTranslation.Y = (playArea.Height + turtleTranslation.Y - s * DisplacementAmount) % playArea.Height;
                return;
            }
            if (turtleTranslation.Y != this.CENTER_Y)
            {                     
                turtleTranslation.Y = this.CENTER_Y;
                return;
            }
            turtleRotation.Angle = 0;
            this.turtleCentering = false;
        }

        private void ZigZagTurtle(int step)
        {
            int count = 0;
            int DisplacementAmount = 60;
            turtleRotation.Angle = 90;
            for (int i = 0; i < 3; i++)
            {
                count++;
                if (count <= step) continue;
                turtleTranslation.X = (playArea.Width + turtleTranslation.X + DisplacementAmount) % playArea.Width;
                return;
            }

            turtleRotation.Angle = 180 + 45;
            for (int i = 0; i < 3; i++)
            {
                count++;
                if (count <= step) continue;
                turtleTranslation.Y = (playArea.Height + turtleTranslation.Y +DisplacementAmount) % playArea.Height;
                turtleTranslation.X = (playArea.Width + turtleTranslation.X - DisplacementAmount) % playArea.Width;
                return;
            }

            turtleRotation.Angle = 90;
            for (int i = 0; i < 3; i++)
            {
                count++;
                if (count <= step) continue;
                turtleTranslation.X = (playArea.Width + turtleTranslation.X + DisplacementAmount) % playArea.Width;
                return;
            }
            this.turtleZigZag = false;
            this.zigzagCount = 1;
        }

        private void RectangleTurtle(int step)
        {
            int DisplacementAmount = 60;
            int count = 0;
            turtleRotation.Angle = 90;
            for (int i = 0; i < 3; i++)
            {
                count++;
                if (count <= step) continue;
                turtleTranslation.X = (playArea.Width + turtleTranslation.X + DisplacementAmount) % playArea.Width;
                return;
            }

            turtleRotation.Angle = 180;
            for (int i = 0; i < 3; i++)
            {
                count++;
                if (count <= step) continue;
                turtleTranslation.Y = (playArea.Height + turtleTranslation.Y + DisplacementAmount) % playArea.Height;
                return;
            }

            turtleRotation.Angle = -90;
            for (int i = 0; i < 3; i++)
            {
                count++;
                if (count <= step) continue;
                turtleTranslation.X = (playArea.Width + turtleTranslation.X - DisplacementAmount) % playArea.Width;
                return;
            }

            turtleRotation.Angle = 0;
            for (int i = 0; i < 3; i++)
            {
                count++;
                if (count <= step) continue;
                turtleTranslation.Y = (playArea.Height + turtleTranslation.Y - DisplacementAmount) % playArea.Height;
                return;
            }
            this.turtleRectangle = false;
            this.rectangleCount = 1;
        }





    }
}