﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Microsoft.Samples.Kinect.SpeechBasics
{
    public delegate void MoveEventHandler (Object sender, MoveEventArgs e);
    public class MoveEventArgs : EventArgs
    {
        public MoveEventArgs(Direction direction)
        {
            this.Verb = direction;
        }

        public Direction Verb { get; set; }
    }
}
